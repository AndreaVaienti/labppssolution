import lab01.tdd.CircularList;
import lab01.tdd.CircularListImpl;

import lab01.tdd.EvenStrategy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
class CircularListTest {

    private CircularList circularList;

    @BeforeEach
    void beforeEach(){
        circularList = new CircularListImpl();
    }

    @Test
    void testCreation() {
        assertTrue(circularList.isEmpty());
    }

    @Test
    void testAddElement(){
        circularList.add(1);
        assertFalse(circularList.isEmpty());
    }

    @Test
    void testListSize(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(2, circularList.size());
    }

    @Test
    void testNotEmpty(){
        circularList.add(1);
        assertFalse(circularList.isEmpty());
    }

    @Test
    void testNext(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        assertEquals(1, circularList.next().get());
        assertEquals(2, circularList.next().get());
        assertEquals(3, circularList.next().get());
        assertEquals(1, circularList.next().get());
        assertEquals(2, circularList.next().get());
        assertEquals(3, circularList.next().get());
    }

    @Test
    void testNextWithEmptyList(){
        assertEquals(Optional.empty(), circularList.next());
    }

    @Test
    void testPrevious(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        assertEquals(3, circularList.previous().get());
        assertEquals(2, circularList.previous().get());
        assertEquals(1, circularList.previous().get());
        assertEquals(3, circularList.previous().get());
        assertEquals(2, circularList.previous().get());
        assertEquals(1, circularList.previous().get());
    }

    @Test
    void testCommonUse(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(1, circularList.next().get());
        assertEquals(2, circularList.next().get());
        assertEquals(1, circularList.next().get());
        circularList.add(3);
        assertEquals(3, circularList.previous().get());
        assertEquals(2, circularList.previous().get());
        circularList.add(4);
        assertEquals(3, circularList.next().get());
        assertEquals(4, circularList.next().get());
        circularList.reset();
        assertEquals(4, circularList.previous().get());
        assertEquals(3, circularList.previous().get());
        circularList.reset();
        assertEquals(1, circularList.next().get());
        assertEquals(2, circularList.next().get());
    }

    @Test
    void testNextWithStrategyNoElements(){
        assertEquals(Optional.empty(), circularList.next(new EvenStrategy()));
    }

    @Test
    void testNextWithStrategy(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        circularList.add(4);
        assertEquals(2, circularList.next(new EvenStrategy()).get());
        assertEquals(4, circularList.next(new EvenStrategy()).get());
    }

}