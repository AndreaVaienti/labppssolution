package lab01.tdd;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;
import java.util.OptionalInt;

public class CircularListImpl implements CircularList{

    private static final int HEAD = 0;
    private static final int RESET_INDEX_POINTER = -1;
    private static final int ADAPT_INDEX = 1;
    private final LinkedList<Integer> list = new LinkedList<>();
    private int pointer = -1;

    @Override
    public void add(int element) { this.list.add(element); }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() { return move(Move.NEXT); }

    @Override
    public Optional<Integer> previous() { return move(Move.PREVIOUS); }

    @Override
    public void reset() { this.pointer = RESET_INDEX_POINTER; }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        for(Integer node: this.list){
            Optional<Integer> value = next();
            if(strategy.apply(value.get())){
                return value;
            }
        }
        return Optional.empty();
    }

    private void updatePointerNext(){
        this.pointer = (this.pointer + 1) % this.list.size();
    }

    private void updatePointerPrevious(){
        this.pointer = this.pointer <= HEAD ? this.list.size() - ADAPT_INDEX : this.pointer - 1;
    }

    private Optional<Integer> move(Move moveIndex){
        if(this.list.isEmpty()) {
            return Optional.empty();
        }

        if(moveIndex.equals(Move.NEXT))
            updatePointerNext();
        else
            updatePointerPrevious();

        return Optional.ofNullable(this.list.get(this.pointer));
    }

}
