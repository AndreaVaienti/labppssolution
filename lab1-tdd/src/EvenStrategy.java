package lab01.tdd;

public class EvenStrategy implements SelectStrategy {

    @Override
    public boolean apply(int element) {
        if(element % 2 == 0)
            return  true;
        return false;
    }
}
